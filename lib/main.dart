import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello World App'),
        ),
        body: Center(
          child: Text("Hello"
              "\nName: Tanya Raghav"
              "\nExpected Graduation Date: Fall 2021"
              "\nFavorite Quote: Life is too short to be sad."),
        ),
      ),
    );
  }
}
